# A Tour of QuickCheck

Welcome! This collection of programs demonstrates how to do
quickcheck-style testing in several programming languages.

Currently included are:

* Haskell
* Clojure
