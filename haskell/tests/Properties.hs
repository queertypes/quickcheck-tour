import Test.QuickCheck

import Reverse

prop_reverseTwiceIdentity :: [Int] -> Bool 
prop_reverseTwiceIdentity = (\xs -> xs == (reverse' . reverse') xs)

main :: IO ()
main = quickCheck prop_reverseTwiceIdentity