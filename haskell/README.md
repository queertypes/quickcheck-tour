# Haskell QuickCheck

A demonstration of using quickcheck in Haskell.

## Files

```
├── LICENSE
├── tests/Properties.hs     -- test suite
├── quick-check-tour.cabal  -- installer metadata: deps, etc.
├── src/Reverse.hs          -- library to be tested
└── Setup.hs                -- trivial installer
```

## Installing

1. Install [haskell](https://github.com/bitemyapp/learnhaskell#getting-started)
2. Make sure your cabal-install (haskell package manager) is at least
   version 1.18

```
$ cabal --version
cabal-install version 1.20.0.3
using version 1.20.0.1 of the Cabal library
```


## Running

```
[quickcheck-tour/haskell] $ cabal sandbox init
[quickcheck-tour/haskell] $ cabal test properties
```
