module Reverse (
  reverse'
) where

reverse' :: [a] -> [a]
reverse' = foldl (flip (:)) []