# Clojure QuickCheck

A demonstration of using
[test.check](https://github.com/clojure/test.check) in Clojure.

## Files

```
├── LICENSE
├── project.clj             -- installer metadata: deps, etc.
├── test/properties.clj     -- test suite
└── src/rev.clj             -- library to be tested
```

## Installing

1. Install [lein](http://leiningen.org/#install)

## Running

```
[quickcheck-tour/clojure] $ lein test
```
