(ns properties
  (:require [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop]
            [rev :refer :all]))

(def reverse-twice-identity-prop
  (prop/for-all [v (gen/vector gen/int)]
                (= v (rev/reverse' (rev/reverse' v))))
  )

(tc/quick-check 100 reverse-twice-identity-prop)
