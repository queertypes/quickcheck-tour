(defproject quick-check-tour "1.0.0"
  :description "QuickCheck in Clojure"
  :license {:name "GPLv3"
            :url "http://www.gnu.org/licenses/gpl.html"}
  :source-paths ["src"]
  :test-paths ["test"]
  :dependencies [[org.clojure/clojure "1.6.0"],
                 [org.clojure/test.check "0.5.9"]]
  )
